// HellowWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include "Helpers.h"
#include <ctime>

int main()
{
	Animal* a[5];

	a[0] = new Cat();
	a[1] = new Dog();
	a[2] = new Cow();
	a[3] = new Chicken();
	a[4] = new Horse();
	
	for (int i = 0; i < 5; i++)
	{
		a[i]->Voice();
	}
}