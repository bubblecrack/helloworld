#include <iostream>

int SquareSum(int a, int b);

template <class T>
class Stack
{
	struct Node
	{
		T val;
		Node* next;
	};
	Node* top;

public:
	Stack()
	{
		top = nullptr;
	}

	Stack(T val)
	{
		top = new Node();
		top->next = nullptr;
		top->val = val;
	}

	void push(T val) {
		Node* newNode = new Node();
		newNode->val = val;
		newNode->next = top;
		top = newNode;
	}

	void pop() 
	{
		if (top)
		{
			Node* oldNode = top;
			top = oldNode->next;
			std::cout << oldNode->val << '\n';
			delete oldNode;
		}
		else
		{
			std::cout << "Stack is empty!!!\n";
		}
	}
};

class Animal
{
public:
	Animal() {}

	virtual void Voice() 
	{
		std::cout << "Base class" << '\n';
	};
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow" << '\n';
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof" << '\n';
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Moo" << '\n';
	}
};

class Chicken : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Cluck" << '\n';
	}
};

class Horse : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Neigh" << '\n';
	}
};